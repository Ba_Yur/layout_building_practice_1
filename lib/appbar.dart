import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          height: 2.0,
          thickness: 2.5,
          color: Colors.black,
        ),
        Container(
          padding: EdgeInsets.only(top: 12.0, bottom: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Custom AppBar'),
              // IconButton(icon: Icon(Icons.menu), onPressed: null)
              Container(
                // margin: EdgeInsets.only(top: 10),
                height: 20.0,
                width: 20.0,
                child: (
                    Column(
                      children: [
                        Divider(
                          height: 5.0,
                          color: Colors.black,
                          thickness: 2.0,
                        ),
                        Divider(
                          height: 5.0,
                          color: Colors.black,
                          indent: 10.0,
                          thickness: 2.0,
                        ),
                        SizedBox(),
                      ],
                    )
                ),
              )
            ],
          ),
        ),
        Divider(
          height: 25.0,
          thickness: 2.5,
          color: Colors.black,
        ),
      ],
    );
  }
}
