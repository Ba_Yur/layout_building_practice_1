import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class Carousel extends StatefulWidget {
  const Carousel({Key key}) : super(key: key);

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: CarouselSlider(
        options: CarouselOptions(
            height: 130.0,
            viewportFraction: 0.22,
            initialPage: 0,
            enableInfiniteScroll: true,
            onPageChanged: (index, reason) {
              setState(() {
                currentPage = index;
              });
            }),
        items: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((i) {
          return Builder(builder: (BuildContext context) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  height: 100,
                  width: 80,
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 2),
                      ]),
                  child: Icon(
                    Icons.android_outlined,
                    size: 35.0,
                    color:
                        currentPage + 1 == i ? Colors.amberAccent : Colors.white70,
                  ),
                ),
                AnimatedOpacity(
                  duration: Duration(milliseconds: 500),
                  opacity: currentPage + 1 == i ? 1.0 : 0.0,
                  child: Container(
                    height: 8,
                    width: 25,
                    decoration: BoxDecoration(
                      color: Colors.amberAccent,
                      borderRadius: BorderRadius.circular(4.0),
                      border: Border.all(
                      )
                    ),
                  ),
                )
              ],
            );
          });
        }).toList(),
      ),
    );
  }
}
