import 'package:flutter/material.dart';
import 'package:layout_building_practice_1/mainLayout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color.fromRGBO(244, 234, 221, 1),
        accentColor: Color.fromRGBO(233, 93, 85, 1),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      Child: Center(
        child: Container(
          child: Text('Some Child'),
        ),
      )
    );
  }
}
