import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:layout_building_practice_1/appbar.dart';

import 'carousel.dart';

class MainLayout extends StatelessWidget {
  //
  final Widget Child;

  const MainLayout({this.Child});

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    // final double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        children: [
          Stack(
            children: [
              Positioned(
                right: -screenWidth * 0.25,
                top: -screenWidth * 0.1,
                child: Container(
                  height: screenWidth * 0.75,
                  width: screenWidth * 0.75,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                right: screenWidth * 0.1,
                top: screenWidth * 0.2,
                child: Container(
                  height: screenWidth * 0.5,
                  width: screenWidth * 0.5,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).accentColor, width: 3),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                right: -screenWidth * 0.12,
                top: screenWidth * 0.33,
                child: Container(
                  height: screenWidth * 0.25,
                  width: screenWidth * 0.25,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).primaryColor, width: 3),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                left: -screenWidth * 0.08,
                bottom: -screenWidth * 0.25,
                child: Container(
                  height: screenWidth * 0.4,
                  width: screenWidth * 0.4,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).accentColor, width: 3),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                left: -screenWidth * 0.13,
                bottom: -screenWidth * 0.13,
                child: Container(
                  height: screenWidth * 0.35,
                  width: screenWidth * 0.35,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                left: -screenWidth * 0.08,
                bottom: screenWidth * 0.14,
                child: Container(
                  height: screenWidth * 0.15,
                  width: screenWidth * 0.15,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).primaryColor, width: 3.0),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            child: Container(
              height: 150.0,
              margin: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30.0),
              child: CustomAppbar(),
            ),
          ),
          Child,
          Positioned(
            bottom: 50,
            child: Carousel(),
          )
        ],
      ),
    );
  }
}
